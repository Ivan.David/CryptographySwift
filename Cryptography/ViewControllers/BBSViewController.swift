//
//  BBSViewController.swift
//  Cryptography
//
//  Created by David Ivan on 05/05/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class BBSViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var pValueTextField: UITextField!
    @IBOutlet weak var nValueTextField: UITextField!
    @IBOutlet weak var sValueTextField: UITextField!
    @IBOutlet weak var qValueTextField: UITextField!

    @IBOutlet weak var tableView: UITableView!

    private var bbsPRNG: BBSAlgorithm?
    private var n: Int = 0
    private var p: Int64 = 0
    private var q: Int64 = 0
    private var seed: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // tableView.register(UITableViewCell.self, forCellReuseIdentifier: "BBSCellReuseIdentifier")
        //self.tableView.registerNib(UINib(nibName: "BBSTableViewCell", bundle: nil))
        self.tableView.register(UINib.init(nibName: "BBSTableViewCell", bundle: nil), forCellReuseIdentifier: "BBSCellReuseIdentifier")

        pValueTextField.delegate = self
        nValueTextField.delegate = self
        sValueTextField.delegate = self
        qValueTextField.delegate = self

       // self.tableView.register(BBSTableViewCell.self, forCellReuseIdentifier: "BBSCellReuseIdentifier")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startButtonTouch(_ sender: Any) {
        bbsPRNG = BBSAlgorithm.init(n: n, p: p, q: q, seed: seed)
        bbsPRNG?.execute()

        tableView.dataSource = bbsPRNG
        tableView.reloadData()
    }

    // MARK: - TextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case pValueTextField:
            p = Int64(textField.text!)!
        case nValueTextField:
            n = Int(textField.text!)!
        case sValueTextField:
            seed = Int(textField.text!)!
        case qValueTextField:
            q = Int64(textField.text!)!
        default:
            break
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case pValueTextField:
            p = Int64(textField.text!)!
        case nValueTextField:
            n = Int(textField.text!)!
        case sValueTextField:
            seed = Int(textField.text!)!
        case qValueTextField:
            q = Int64(textField.text!)!
        default:
            break
        }

        textField.resignFirstResponder()
        return true
    }

}
