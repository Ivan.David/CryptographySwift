//
//  BBSTableViewCell.swift
//  Cryptography
//
//  Created by David Ivan on 05/05/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class BBSTableViewCell: UITableViewCell {

    @IBOutlet weak var lsbLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
