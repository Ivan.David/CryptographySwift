//
//  CezarDecryptAlgorithm.swift
//  Cryptography
//
//  Created by David Ivan on 25/03/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

// Performs the Cezar decrypt brute force algorithm, returning an array of all the

import UIKit

extension Character {
    var asciiValue: UInt32? {
        return String(self).unicodeScalars.filter{$0.isASCII}.first?.value
    }
}

class CezarDecryptAlgorithm: NSObject, UITableViewDataSource {
    private let encryptedString: String
    private var decryptedStringArray: Array<String> = []

    private let cellReuseIdentifier = "cellReuseIdentifier"

    init(encryptedString: String) {
        self.encryptedString = encryptedString.lowercased().trimmingCharacters(in: .whitespaces)
    }

    func bruteForceAttack() {
        let letters = CharacterSet.letters

        for shiftValue in 1...25 {
            var decryptedString = ""
            for letter in encryptedString {
                if letters.contains(letter.unicodeScalars.map { $0 }.first!) {
                    guard var letterASCIIValue = letter.asciiValue else { return assertionFailure("Invalid String")}

                    // 97 - value of 'a' in the ASCII table
                    letterASCIIValue =  letterASCIIValue - 97
                    letterASCIIValue = letterASCIIValue + UInt32(shiftValue)
                    // 26 - number of letters in the english alphabet
                    letterASCIIValue = letterASCIIValue % 26

                    let shiftedLetter = Character(UnicodeScalar(letterASCIIValue + 97)!)

                    decryptedString.append(shiftedLetter)
                }
            }
            decryptedStringArray.append(decryptedString)
        }
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        //guard let decryptedStringArray = decryptedStringArray else { return cell }
        cell.textLabel?.text = String(format: "Key %d - %@", indexPath.row + 1, decryptedStringArray[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //guard let numberOfRows = decryptedStringArray.count else { return 1 }
        return decryptedStringArray.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
