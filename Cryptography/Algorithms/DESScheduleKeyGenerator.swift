//
//  DESScheduleKeyGenerator.swift
//  Cryptography
//
//  Created by David Ivan on 27/03/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class DESScheduleKeyGenerator: NSObject, UITableViewDataSource {
    //private let hardcodedKey = "PASSWORD"
    private let key: String

    private let permuteChoiceOneMatrix = [ 57, 49, 41, 33, 25, 17, 9,
                                           1, 58, 50, 42, 34, 26, 18,
                                           10, 2, 59, 51, 43, 35, 27,
                                           19, 11, 3, 60, 52, 44, 36,
                                           63, 55, 47, 39, 31, 23, 15,
                                           7, 62, 54, 46, 38, 30, 22,
                                           14, 6, 61, 53, 45, 37, 29,
                                           21, 13, 5, 28, 20, 12, 4
                                        ]
    private let permuteChoiceTwoMatrix = [ 14, 17, 11, 24, 1, 5,
                                           3, 28, 15, 6, 21, 10,
                                           23, 19, 12, 4, 26, 8,
                                           16, 7, 27, 20, 13, 2,
                                           41, 52, 31, 37, 47, 55,
                                           30, 40, 51, 45, 33, 48,
                                           44, 49, 39, 56, 34, 53,
                                           46, 42, 50, 36, 29, 32
                                        ]

    private var roundKeysArray: Array<String> = []
    var binaryKey = ""
    private let cellReuseIdentifier = "DEScellReuseIdentifier"

    init(key: String) {
        self.key = key
    }

    // For iteration number 1, we have 1 one left shift, for iteration number 3, we have 2 left shifts
    private let numberOfLeftShiftsPerIteration = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]

    private func parseKeyIntoBinary(key:String) -> String {
        // Will transform the key into a binary string containing all the letters of the key as 8 bit binary
        var binaryKey = ""
        for character in key {
            guard let characterAsciiValue = character.asciiValue else { assertionFailure("Invalid key"); return "" }
            binaryKey.append(String(characterAsciiValue, radix: 2).pad(with: "0", toLength: 8))
        }
        self.binaryKey = binaryKey
        return binaryKey

//        // Array of characters
//        let characterArray = [Character](key)
//
//        // Array of asccii value
//        let asciiArray = characterArray.map({String($0).unicodeScalars.first!.value})
//
//        // Array of binary value
//        let binaryArray = asciiArray.map ({ String($0, radix: 2).pad(with: "0", toLength: 8)})
//
//        // Reduce in a String
//        let binaryKey = binaryArray.reduce("",{$0 + $1})
//
//        self.binaryKey = binaryKey

//        return binaryKey
    }

//    private func convertStringTo64X8byteMatrix(string: String) -> Array<String> {
//        assert(string.count == 64*8, "The string needs to have 8 letters in binary")
//        var mutableMatrix: Array<String> = []
//
//        for i in 0...63 {
//            let start = string.index(string.startIndex, offsetBy: 8*i)
//            let end = string.index(string.endIndex, offsetBy: 2*8-1)
//            let range = start..<end
//
//            mutableMatrix[i] = String(string[range])
//        }
//
//        return mutableMatrix
//    }

    private func applyPermuteChoiceMatrices(numberOfPermuteMatrix string: String, byteMatrix: String) -> String {
        assert(string == "one" || string == "two", "Please provide an adequate permuteChoiceMatrix number")

        var permuteMatrix: [Int] = []
        var permutedMatrix: String = ""

        if string == "one" {
            permuteMatrix = permuteChoiceOneMatrix

            for i in 0..<56 {
                let index = byteMatrix.index(byteMatrix.startIndex, offsetBy: permuteMatrix[i] - 1)
                let byte = byteMatrix[index]
                permutedMatrix.append(byte)
            }
        } else if string == "two" {
            permuteMatrix = permuteChoiceTwoMatrix

            for i in 0..<48 {
                let index = byteMatrix.index(byteMatrix.startIndex, offsetBy: permuteMatrix[i] - 1)
                let byte = byteMatrix[index]
                permutedMatrix.append(byte)
            }
        }

        return permutedMatrix
    }

    private func applyLeftShift(string: String, numberOfShifts: Int) -> String {
        assert(numberOfShifts == 1 || numberOfShifts == 2, "Wrong number of shifts")

        var shiftedString = shiftLeft(string: string)

        if (numberOfShifts == 2) {
            shiftedString = shiftLeft(string: shiftedString)
        }

        return shiftedString
    }

    private func shiftLeft(string: String) -> String{
        // Could work as a String extension
       let firstByte = string[string.startIndex]

        var shiftedString: String = ""

        for i in 1..<string.count {
            let start = string.index(string.startIndex, offsetBy: i)
            shiftedString.append(string[start])
        }

        shiftedString.append(firstByte)

        return shiftedString
    }

    private func convertBinaryStringToASCII (binaryString: String) -> String {
        var index = binaryString.startIndex
        var result: String = ""
        for _ in 0..<binaryString.count/8 {
            let nextIndex = binaryString.index(index, offsetBy: 8)
            let charBits = binaryString[index..<nextIndex]
            result += String(UnicodeScalar(UInt8(charBits, radix: 2)!))
            index = nextIndex
        }

        return result
    }

    func generateRoundKeys() {
        let binaryKey = parseKeyIntoBinary(key: key)

        var permutedBinaryKey = applyPermuteChoiceMatrices(numberOfPermuteMatrix: "one", byteMatrix: binaryKey)

        for i in 0...15 {
            let start = permutedBinaryKey.index(permutedBinaryKey.startIndex, offsetBy: 0)
            let end = permutedBinaryKey.index(permutedBinaryKey.startIndex, offsetBy: 28)
            let range = start..<end
            let leftSide = permutedBinaryKey[range]

            let start2 = permutedBinaryKey.index(permutedBinaryKey.startIndex, offsetBy: 28)
            let end2 = permutedBinaryKey.endIndex
            let range2 = start2..<end2
            let rightSide = permutedBinaryKey[range2]

            let shiftedLeftSide = applyLeftShift(string: String(leftSide), numberOfShifts: numberOfLeftShiftsPerIteration[i])
            let shiftedRightSide = applyLeftShift(string: String(rightSide), numberOfShifts: numberOfLeftShiftsPerIteration[i])

            permutedBinaryKey = shiftedLeftSide + shiftedRightSide
            //let asciiString = convertBinaryStringToASCII(binaryString: permutedBinaryKey)

            //let hexaString = String(Int(permutedBinaryKey, radix: 2)!, radix: 16)

            let permutedKey = applyPermuteChoiceMatrices(numberOfPermuteMatrix: "two", byteMatrix: permutedBinaryKey)

            var hexaString = ""
            for i in 0..<12 {
                let start = permutedKey.index(permutedBinaryKey.startIndex, offsetBy: i*4)
                let end = permutedKey.index(permutedBinaryKey.startIndex, offsetBy: (i+1)*4)
                let range = start..<end
                let leftSide = permutedKey[range]

                hexaString += String(Int(leftSide, radix: 2)!, radix: 16)
            }
            
            roundKeysArray.append(hexaString)
            //transformat permutedBinaryKey in ASCII
        }
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        cell.textLabel?.text = String(format: "Key %d - %@", indexPath.row + 1, roundKeysArray[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // guard let numberOfRows = decryptedStringArray.count else { return 1 }
        return roundKeysArray.count
       // return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
