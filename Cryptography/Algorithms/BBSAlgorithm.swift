//
//  BBSAlgorithm.swift
//  Cryptography
//
//  Created by David Ivan on 05/05/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class BBSAlgorithm: NSObject, UITableViewDataSource {

     private let identifier = "BBSCellReuseIdentifier"

    private let n: Int
    private let p: Int64
    private let q: Int64
    private let seed: Int

    private let m: Int64

    private var generatedNumbersArray: Array<Int64> = []
    private var lsbArray: Array<Int> = []

    init(n: Int, p: Int64, q: Int64, seed: Int) {
        self.n = n
        self.p = p
        self.q = q
        self.seed = seed

        self.m = p*q
        self.generatedNumbersArray.append(Int64(seed))
        self.lsbArray.append(seed % 2)
    }

    func execute() {
        for i in 1...n {
            let previousNumber = generatedNumbersArray[i-1]
            let generatedNumber = (previousNumber*previousNumber) % m
            generatedNumbersArray.append(generatedNumber)
            lsbArray.append(Int(generatedNumber % 2))
        }
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: BBSTableViewCell
        if let dequeuedcell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? BBSTableViewCell {
            cell = dequeuedcell
        } else {
            cell = BBSTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: identifier)
        }

        cell.numberLabel.text = String(format: "Nr: %d", generatedNumbersArray[indexPath.row + 1])
        cell.lsbLabel.text = String(format: "LSB: %d", lsbArray[indexPath.row + 1])

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return generatedNumbersArray.count - 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
