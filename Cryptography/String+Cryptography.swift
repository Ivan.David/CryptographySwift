//
//  String+Cryptography.swift
//  Cryptography
//
//  Created by David Ivan on 06/04/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import Foundation

extension String {
    func parseHexaToInt() -> Int? {
        assert(self.count == 1, "Must be applied on one letter string")
        if let characterAsInt = Int(self) {
            return characterAsInt
        } else {
            let charAsInt: Int?
            switch self {
            case "A", "a":
            //case "a":
                charAsInt = 10
            case "b", "B":
                charAsInt = 11
            case "C", "c":
                charAsInt = 12
            case "D", "d":
                charAsInt = 12
            case "E", "e":
                charAsInt = 14
            case "F", "f":
                charAsInt = 15
            default:
                charAsInt = nil
                assertionFailure("Invalid hexa value")
            }
            return charAsInt
        }
    }

    func pad(with character: String, toLength length: Int) -> String {
        let padCount = length - self.count
        guard padCount > 0 else { return self }

        //return String(describing: repeatElement(character, count: padCount)) + self

        var weakSelf = self
        for _ in 0..<padCount {
            weakSelf.insert("0", at: self.startIndex)
        }

        return weakSelf
    }

    func parseIntoBinary() -> String {
        // Will transform the key into a binary string containing all the letters of the key as 8 bit binary
        var binaryKey = ""
        for character in self {
            guard let characterAsciiValue = character.asciiValue else { assertionFailure("Invalid String"); return "" }
            binaryKey.append(String(characterAsciiValue, radix: 2).pad(with: "0", toLength: 8))
        }
        return binaryKey
    }

    func parseIntoHexa() -> String {
        // Will transform the key into a binary string containing all the letters of the key as 8 bit binary
        var hexaKey = ""
        for character in self {
            guard let characterAsciiValue = character.asciiValue else { assertionFailure("Invalid String"); return "" }
            hexaKey.append(String(characterAsciiValue, radix: 16))
        }
        return hexaKey
    }

    func convertBinaryStringToASCII() -> String {
        var index = self.startIndex
        var result: String = ""
        for _ in 0..<self.count/8 {
            let nextIndex = self.index(index, offsetBy: 8)
            let charBits = self[index..<nextIndex]
            result += String(UnicodeScalar(UInt8(charBits, radix: 2)!))
            index = nextIndex
        }

        return result
    }

    func applyLeftShift(numberOfShifts: Int) -> String {
        assert(numberOfShifts == 1 || numberOfShifts == 2, "Wrong number of shifts")

        var shiftedString = shiftLeft(string: self)

        if (numberOfShifts == 2) {
            shiftedString = shiftLeft(string: shiftedString)
        }

        return shiftedString
    }

    private func shiftLeft(string: String) -> String{
        // Could work as a String extension
        let firstByte = string[string.startIndex]

        var shiftedString: String = ""

        for i in 1..<string.count {
            let start = string.index(string.startIndex, offsetBy: i)
            shiftedString.append(string[start])
        }

        shiftedString.append(firstByte)

        return shiftedString
    }
}
