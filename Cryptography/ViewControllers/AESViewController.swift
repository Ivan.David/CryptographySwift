//
//  AESViewController.swift
//  Cryptography
//
//  Created by David Ivan on 06/04/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class AESViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var tableView: UITableView!

    private var aesKeyScheduleGenerator: AESKeyScheduleKeyGenerator?

    private var key = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "AEScellReuseIdentifier")

        navigationController?.navigationBar.isHidden = false
        titleLabel.text = "AES Round Key Generator"
        textField.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChange(notification:)), name: .UITextFieldTextDidChange, object: nil)
    }
    

    @IBAction func startButtonTouched(_ sender: Any) {
        aesKeyScheduleGenerator = AESKeyScheduleKeyGenerator()
        aesKeyScheduleGenerator?.execute()

        tableView.dataSource = aesKeyScheduleGenerator
        tableView.reloadData()
    }

    @objc func textFieldDidChange(notification: NSNotification) {
        guard let textField = notification.object as? UITextField else { return }

        if let textFieldText = textField.text {
            key = textFieldText
        }
    }

    // MARK: - TextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        key = textField.text!
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let textFieldText = textField.text {
            key = textFieldText
        }

        textField.resignFirstResponder()
        return true
    }

}
