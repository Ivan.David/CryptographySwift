//
//  DESViewController.swift
//  Cryptography
//
//  Created by David Ivan on 26/03/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class DESViewController: UIViewController, UITextFieldDelegate {

    private var desScheduleGenerator: DESScheduleKeyGenerator?

    @IBOutlet private weak var tableview: UITableView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var textField: UITextField!

    private var key = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableview.register(UITableViewCell.self, forCellReuseIdentifier: "DEScellReuseIdentifier")

        navigationController?.navigationBar.isHidden = false
        titleLabel.text = "DES Round Key Generator"
        textField.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(textFieldDidChange(notification:)), name: .UITextFieldTextDidChange, object: nil)
    }

    @IBAction func startButtonTouched(_ sender: Any) {
        desScheduleGenerator = DESScheduleKeyGenerator.init(key: key)
        guard let desScheduleGenerator = desScheduleGenerator else { return }

        desScheduleGenerator.generateRoundKeys()
        tableview.dataSource = desScheduleGenerator
        tableview.reloadData()
    }

    @objc func textFieldDidChange(notification: NSNotification) {
        guard let textField = notification.object as? UITextField else { return }

        if let textFieldText = textField.text {
            key = textFieldText
        }
    }

    // MARK: - TextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        key = textField.text!
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let textFieldText = textField.text {
            key = textFieldText
        }

        textField.resignFirstResponder()
        return true
    }
}
