//
//  CezarViewController.swift
//  Cryptography
//
//  Created by David Ivan on 25/03/2018.
//  Copyright © 2018 David Ivan. All rights reserved.
//

import UIKit

class CezarViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate {

    @IBOutlet private weak var cryptedMessageTextField: UITextField!
    @IBOutlet private weak var tableView: UITableView!

    private var cesarAlgorithm: CezarDecryptAlgorithm? = nil
    private var encryptedString: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellReuseIdentifier")
        cryptedMessageTextField.delegate = self
        
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onStartButtonPressed(_ sender: Any) {
        guard let encryptedString = encryptedString else {
            let alert = UIAlertController(title: "Incorrect String", message: "Please insert a valid encrypted string", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            return
        }

        cesarAlgorithm = CezarDecryptAlgorithm.init(encryptedString: encryptedString)
        cesarAlgorithm?.bruteForceAttack()
        tableView.dataSource = cesarAlgorithm
        tableView.reloadData()
    }

 // MARK: - TextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        encryptedString = textField.text
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        encryptedString = textField.text
        textField.resignFirstResponder()
        return true
    }
}
